<?php
declare(strict_types=1);

namespace Maxipost\FormStrategyTest;

use DateTimeImmutable;
use Maxipost\FormStrategy\FormStrategyBuilder;
use PHPUnit\Framework\TestCase;

class FormStrategyBuilderTest extends TestCase
{

    private const TEST_TIMESTAMP = 1551257219;
    private const TEST_DATETIME = '2019-02-27T09:16:59+00:00';

    public function testHydrate(): void
    {
        $result = (new FormStrategyBuilder)->buildFromArray(
            [
                FormStrategyBuilder::DTO => TestClass\TestClass::class,
                FormStrategyBuilder::NESTED_FIELDS => [
                    'testField1' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                    ],
                    'testField2' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'testField3' => [FormStrategyBuilder::DTO => TestClass\TestClass::class],
                        ],
                    ],
                    'testField4' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                        FormStrategyBuilder::IS_ARRAY => true,
                    ],
                    'testField5Single' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                        FormStrategyBuilder::SINGLE_NAME => 'test',
                    ],
                    'testField6Time' => [
                        FormStrategyBuilder::DTO => DateTimeImmutable::class,
                        FormStrategyBuilder::HYDRATE_CALLBACK => static function ($value) {
                            return new DateTimeImmutable($value['time']);
                        },
                    ],
                ],
            ]
        )->hydrate(
            [
                'testField1' => ['test' => 'test'],
                'testField2' => [
                    'test' => 'test',
                    'testField3' => ['test' => 'test'],
                ],
                'testField4' => [
                    ['test' => 'test'],
                    ['test' => 'test'],
                ],
                'testField5Single' => 'single',
                'testField6Time' => [
                    'time' => self::TEST_DATETIME,
                ],
            ]
        );

        $expected = new TestClass\TestClass;
        $expected->testField1 = new TestClass\TestClass;
        $expected->testField1->test = 'test';
        $expected->testField2 = new TestClass\TestClass;
        $expected->testField2->test = 'test';
        $expected->testField2->testField3 = new TestClass\TestClass;
        $expected->testField2->testField3->test = 'test';
        $testClass = new TestClass\TestClass;
        $testClass->test = 'test';
        $expected->testField4 = [
            $testClass,
            $testClass,
        ];
        $expected->testField5Single = new TestClass\TestClass;
        $expected->testField5Single->test = 'single';
        $expected->testField6Time = new DateTimeImmutable(self::TEST_DATETIME);

        self::assertEquals($expected, $result);
    }

    public function testExtract(): void
    {
        $expectedData = [
            'testField1' => [
                'test' => 'test',
                'testField1' => null,
                'testField2' => null,
                'testField3' => null,
                'testField4' => null,
                'testField5Single' => null,
                'testField6Time' => null,
            ],
            'testField2' => [
                'test' => 'test',
                'testField3' => [
                    'test' => 'test',
                    'testField1' => null,
                    'testField2' => null,
                    'testField3' => null,
                    'testField4' => null,
                    'testField5Single' => null,
                    'testField6Time' => null,
                ],
                'testField1' => null,
                'testField2' => null,
                'testField4' => null,
                'testField5Single' => null,
                'testField6Time' => null,
            ],
            'testField4' => [
                [
                    'test' => 'test',
                    'testField1' => null,
                    'testField2' => null,
                    'testField3' => null,
                    'testField4' => null,
                    'testField5Single' => null,
                    'testField6Time' => null,
                ],
                [
                    'test' => 'test',
                    'testField1' => null,
                    'testField2' => null,
                    'testField3' => null,
                    'testField4' => null,
                    'testField5Single' => null,
                    'testField6Time' => null,
                ],
            ],
            'testField6Time' => self::TEST_DATETIME,
            'testField5Single' => 'single',
            'test' => null,
            'testField3' => null,

        ];

        $data = new TestClass\TestClass;
        $data->testField1 = new TestClass\TestClass;
        $data->testField1->test = 'test';
        $data->testField2 = new TestClass\TestClass;
        $data->testField2->test = 'test';
        $data->testField2->testField3 = new TestClass\TestClass;
        $data->testField2->testField3->test = 'test';
        $testClass = new TestClass\TestClass;
        $testClass->test = 'test';
        $data->testField4 = [
            $testClass,
            $testClass,
        ];
        $data->testField5Single = new TestClass\TestClass;
        $data->testField5Single->test = 'single';
        $data->testField6Time = new DateTimeImmutable(self::TEST_DATETIME);

        $result = (new FormStrategyBuilder)->buildFromArray(
            [
                FormStrategyBuilder::DTO => TestClass\TestClass::class,
                FormStrategyBuilder::NESTED_FIELDS => [
                    'testField1' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                    ],
                    'testField2' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'testField3' => [FormStrategyBuilder::DTO => TestClass\TestClass::class],
                        ],
                    ],
                    'testField4' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                        FormStrategyBuilder::IS_ARRAY => true,
                    ],
                    'testField5Single' => [
                        FormStrategyBuilder::DTO => TestClass\TestClass::class,
                        FormStrategyBuilder::SINGLE_NAME => 'test',
                    ],
                    'testField6Time' => [
                        FormStrategyBuilder::DTO => DateTimeImmutable::class,
                        FormStrategyBuilder::EXTRACT_CALLBACK => static function (
                            DateTimeImmutable $value
                        ) {
                            return $value->format(DATE_ATOM);
                        },
                    ],
                ],
            ]
        )->extract(
            $data
        );


        self::assertEquals($expectedData, $result);
    }
}
