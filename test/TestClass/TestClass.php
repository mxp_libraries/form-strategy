<?php
declare(strict_types=1);

namespace Maxipost\FormStrategyTest\TestClass;


class TestClass
{
    public $test;
    public $testField1;
    public $testField2;
    public $testField3;
    public $testField4;
    public $testField5Single;
    public $testField6Time;
}