<?php
declare(strict_types=1);

namespace Maxipost\FormStrategy;

use ReflectionClass;
use Zend\Hydrator\Reflection;

trait StrategyTrait
{
    private $hydrator;
    private $dto;
    private $formStrategyBuilder;
    private $singleName;
    private $extractCallback;
    private $hydrateCallback;
    private $isNeedToHydrateIfEmpty;

    public function __construct(
        string $dto,
        FormStrategyBuilder $formStrategyBuilder,
        string $singleName = null,
        callable $extractCallback = null,
        callable $hydrateCallback = null,
        bool $isNeedToHydrateIfEmpty = true
    ) {
        $this->dto = $dto;
        $this->hydrator = new Reflection();
        $this->formStrategyBuilder = $formStrategyBuilder;
        $this->singleName = $singleName;
        $this->extractCallback = $extractCallback;
        $this->hydrateCallback = $hydrateCallback;
        $this->isNeedToHydrateIfEmpty = $isNeedToHydrateIfEmpty;
    }

    public function withNestedFields(array $nestedFields): self
    {
        foreach ($nestedFields as $fieldName => $fieldParams) {
            $this->hydrator->addStrategy(
                $fieldName, $this->formStrategyBuilder->buildFromArray($fieldParams)
            );
        }
        return $this;
    }

    /**
     * @param mixed $data
     * @return object|null
     * @throws \ReflectionException
     */
    private function baseHydrate($data)
    {
        if ($this->hydrateCallback) {
            return call_user_func($this->hydrateCallback, $data, $this->dto);
        }

        if($this->isNeedToHydrateIfEmpty === false && $data === null){
            return null;
        }

        if ($this->singleName !== null) {
            $data = [$this->singleName => $data];
        }

        return $this->hydrator->hydrate(
            $data,
            (new \ReflectionClass($this->dto))->newInstanceWithoutConstructor()
        );
    }

    /**
     * @param mixed $data
     * @return mixed
     * @throws \ReflectionException
     */
    private function baseExtract($data)
    {
        if ($data === null) {
            return null;
        }

        if ($this->extractCallback) {
            return call_user_func($this->extractCallback, $data, $this->dto);
        }

        if ($this->singleName !== null) {
            $reflection = new ReflectionClass($this->dto);
            $property = $reflection->getProperty($this->singleName);
            $property->setAccessible(true);
            return $property->getValue($data);
        }

        return $this->hydrator->extract(
            $data
        );
    }
}