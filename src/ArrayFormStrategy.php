<?php
declare(strict_types=1);

namespace Maxipost\FormStrategy;

use Zend\Hydrator\Exception\InvalidArgumentException;
use Zend\Hydrator\Strategy\StrategyInterface;

class ArrayFormStrategy implements StrategyInterface
{
    use StrategyTrait;

    public function extract($value)
    {
        if (!is_array($value)) {
            return $value;
//            throw new InvalidArgumentException(
//              sprintf(
//                'Value needs to be an array, got "%s" instead.',
//                is_object($value) ? get_class($value) : gettype($value)
//              )
//            );
        }

        return array_map([$this, 'baseExtract'], $value);
    }

    public function hydrate($value)
    {
        if (!is_array($value)) {
            return $value;
//            throw new InvalidArgumentException(
//                sprintf(
//                    'Value needs to be an array, got "%s" instead.',
//                    is_object($value) ? get_class($value) : gettype($value)
//                )
//            );
        }

        return array_map([$this, 'baseHydrate'], $value);
    }
}