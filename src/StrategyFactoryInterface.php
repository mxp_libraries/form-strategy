<?php
declare(strict_types=1);

namespace Maxipost\FormStrategy;

use Zend\Hydrator\Strategy\StrategyInterface;

interface StrategyFactoryInterface
{
    public function __invoke(string $rootClassName): StrategyInterface;
}