<?php
declare(strict_types=1);

namespace Maxipost\FormStrategy;

use Zend\Hydrator\Strategy\StrategyInterface;

class FormStrategyBuilder
{
    public const NESTED_FIELDS = 'nestedFields';
    public const DTO = 'dto';
    public const IS_ARRAY = 'isArray';
    public const SINGLE_NAME = 'singleName';
    public const IS_NEED_TO_HYDRATE_IF_EMPTY = 'isNeedToHydrateIfEmpty';
    public const EXTRACT_CALLBACK = 'extractCallback';
    public const HYDRATE_CALLBACK = 'hydrateCallback';

    public function buildFromArray(array $fieldParams): StrategyInterface
    {
        if (($fieldParams[self::IS_ARRAY] ?? false) === false) {
            $strategy = new DefaultFormStrategy(
                $fieldParams[self::DTO],
                $this,
                $fieldParams[self::SINGLE_NAME] ?? null,
                $fieldParams[self::EXTRACT_CALLBACK] ?? null,
                $fieldParams[self::HYDRATE_CALLBACK] ?? null,
                $fieldParams[self::IS_NEED_TO_HYDRATE_IF_EMPTY] ?? true
            );
        } else {
            $strategy = new ArrayFormStrategy(
                $fieldParams[self::DTO],
                $this,
                $fieldParams[self::SINGLE_NAME] ?? null,
                $fieldParams[self::EXTRACT_CALLBACK] ?? null,
                $fieldParams[self::HYDRATE_CALLBACK] ?? null,
                $fieldParams[self::IS_NEED_TO_HYDRATE_IF_EMPTY] ?? true
            );
        }

        if (isset($fieldParams[self::NESTED_FIELDS])) {
            $strategy->withNestedFields($fieldParams[self::NESTED_FIELDS]);
        }

        return $strategy;//
    }
}