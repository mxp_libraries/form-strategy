<?php
declare(strict_types=1);

namespace Maxipost\FormStrategy;

use Zend\Hydrator\Strategy\StrategyInterface;

class DefaultFormStrategy implements StrategyInterface
{
    use StrategyTrait;

    public function extract($value)
    {
        return $this->baseExtract($value);
    }

    /**
     * @param mixed $data
     * @return mixed|object
     * @throws \ReflectionException
     */
    public function hydrate($data)
    {
        return $this->baseHydrate($data);
    }
}