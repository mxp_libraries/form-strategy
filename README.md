Install
-------------
```bash
composer require maxipost/form-strategy-lib
```

Example hydrate
---------------------------------------------
```php
(new FormStrategyBuilder)->buildFromArray(
    [
        FormStrategyBuilder::DTO => TestClass\TestClass::class,
        FormStrategyBuilder::NESTED_FIELDS => [
            'testField1' => [
                FormStrategyBuilder::DTO => TestClass\TestClass::class,
            ],
            'testField2' => [
                FormStrategyBuilder::DTO => TestClass\TestClass::class,
                FormStrategyBuilder::NESTED_FIELDS => [
                    'testField3' => [FormStrategyBuilder::DTO => TestClass\TestClass::class],
                ],
            ],
            'testField4' => [
                FormStrategyBuilder::DTO => TestClass\TestClass::class,
                FormStrategyBuilder::IS_ARRAY => true,
            ],
            'testField5Single' => [
                FormStrategyBuilder::DTO => TestClass\TestClass::class,
                FormStrategyBuilder::SINGLE_NAME => 'test',
            ],
        ],
    ]
)->hydrate([
        'testField1' => ['test' => 'test'],
        'testField2' => [
            'test' => 'test',
            'testField3' => ['test' => 'test'],
        ],
        'testField4' => [
            ['test' => 'test'],
            ['test' => 'test'],
        ],
        'testField5Single' => 'single',
]);
```


Example extract
---------------------------------------------
```php
(new FormStrategyBuilder)->buildFromArray([
    FormStrategyBuilder::DTO => TestClass\TestClass::class,
    FormStrategyBuilder::NESTED_FIELDS => [
        'testField1' => [
            FormStrategyBuilder::DTO => TestClass\TestClass::class,
        ],
        'testField2' => [
            FormStrategyBuilder::DTO => TestClass\TestClass::class,
            FormStrategyBuilder::NESTED_FIELDS => [
                'testField3' => [FormStrategyBuilder::DTO => TestClass\TestClass::class],
            ],
        ],
        'testField4' => [
            FormStrategyBuilder::DTO => TestClass\TestClass::class,
            FormStrategyBuilder::IS_ARRAY => true,
        ],
        'testField5Single' => [
            FormStrategyBuilder::DTO => TestClass\TestClass::class,
            FormStrategyBuilder::SINGLE_NAME => 'test',
        ],
    ],
])->extract(
    $data
);
```


